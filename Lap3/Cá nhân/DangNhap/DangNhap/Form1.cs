﻿using System.Data;
using System.Data.SqlClient;
namespace DangNhap
{
    public partial class Form1 : Form
    {
        string strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV;Integrated Security=True";
        SqlConnection sqlconn = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnlogin_Click(object sender, EventArgs e)
        {
            
            var TaiKhoan = txtLogin.Text.Trim();  
            var MatKhau = txtpassword.Text.Trim();
            
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
            
            SqlCommand cmd_NV = new SqlCommand("SP_CHECK_NV", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd_NV.Parameters.Add("@TENDN",SqlDbType.VarChar).Value = TaiKhoan;
            cmd_NV.Parameters.Add("@MATKHAU",SqlDbType.VarChar).Value=MatKhau;               

            SqlCommand cmd_SV = new SqlCommand("SP_CHECK_SV", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd_SV.Parameters.Add("@TENDN", SqlDbType.VarChar).Value = TaiKhoan;
            cmd_SV.Parameters.Add("@MATKHAU", SqlDbType.VarChar).Value = MatKhau;
            sqlconn.Open();            
            using SqlDataReader kq_SV = cmd_SV.ExecuteReader();
            
            if (kq_SV.Read())
            {
                MessageBox.Show("Đăng Nhập thành công " + kq_SV[0]);
            }
            else
            {
                sqlconn.Close();

                sqlconn.Open();
                using SqlDataReader kq_NV = cmd_NV.ExecuteReader();
                if (kq_NV.Read())
                {
                    MessageBox.Show("Đăng Nhập thành công" + kq_NV[0]);
                }
                else
                {
                    MessageBox.Show("Tên đăng nhập và mật khẩu không hợp lệ");
                }
                sqlconn.Close();
            }
            







        }

        
    }
}