﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace DangNhap
{
    public partial class frmManagerStudent : Form
    {
        String strConn = @"Data Source=TINHAO-PC;Initial Catalog=QLSV_NHOM;Integrated Security=True;MultipleActiveResultSets=True";
        SqlConnection sqlconn = null;
        SqlCommand cmd;
        string MANV,MALOP;
        public frmManagerStudent(string Manv,string Malop)
        {
            this.MANV = Manv;
            this.MALOP = Malop;
            InitializeComponent();
            if (sqlconn == null)
            {
                sqlconn = new SqlConnection(strConn);
            }
        }

        private void frmManagerStudent_Load(object sender, EventArgs e)
        {
            dgvSinhVien_Data();
            cbxNhanVien.DisplayMember = "HOTEN";
            cbxNhanVien.DataSource = fetchNameNV();
            cbxLopHoc.DisplayMember = "TENLOP";
            cbxLopHoc.DataSource = fetchNameLOP();
            txtSoLuong.Text = fetchSoLuongSinhVien(MALOP).ToString();
        }
        private void dgvSinhVien_Data()
        {
            dgvSinhVien.DataSource = fetchSinhVien();
        }

        private void dgvSinhVien_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow dataGridViewRow = dgvSinhVien.Rows[e.RowIndex];

        }

        private DataTable fetchSinhVien()
        {
            DataTable dt = new DataTable();
            if(sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_SINHVIEN", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 20).Value = MALOP;
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dt);
            return dt;
        }
        private DataTable fetchNameNV()
        {
            DataTable dt = new DataTable();
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_NAME_NV", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            
            sqlDataAdapter.Fill(dt);
            return dt;
        }
        private DataTable fetchNameLOP()
        {
            DataTable dt = new DataTable();
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_SEL_NAME_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
            sqlDataAdapter.Fill(dt);
            return dt;
        }
        private int fetchSoLuongSinhVien(string Malop)
        {
            if (sqlconn.State == ConnectionState.Closed)
            {
                sqlconn.Open();
            }
            cmd = new SqlCommand("SP_CNT_SV_LOP", sqlconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("MALOP", SqlDbType.VarChar, 50).Value = Malop;
            SqlDataReader kq = cmd.ExecuteReader();
            if (kq.Read())
            {
                return kq.GetInt32(0);
            }
            else
            {
                return 0;
            }
        }
    }
}
